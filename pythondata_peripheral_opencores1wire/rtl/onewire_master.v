// © 2020 - 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

// Stop LiteX silently ignoring net naming / missing register errors
`default_nettype none

module onewire_master_wishbone (
		// Configuration registers
		input wire [31:0] sys_clk_freq,

		// Wishbone slave port signals
		input wire slave_wb_cyc,
		input wire slave_wb_stb,
		input wire slave_wb_we,
		input wire [4:0] slave_wb_addr,
		input wire [7:0] slave_wb_dat_w,
		output wire [7:0] slave_wb_dat_r,
		output wire slave_wb_ack,
		output wire slave_wb_err,
		output wire slave_irq_o,

		input wire peripheral_reset,
		input wire peripheral_clock,

		// OneWire interface
		input  owi_i,     // input from bidirectional wire
		output owi_o,    // output to bidirectional wire (locked to 0)
		output owi_oe,   // output pull down enable
		output owi_p     // output power enable
	);

	assign owi_o = 1'b0;

	// Avalon bus signals
	wire avalon_wen;
	wire avalon_ren;
	wire [4:0] avalon_adr;
	wire [7:0] avalon_wdt;
	wire [7:0] avalon_rdt;

	// Instantiate sockit bus converter module and connect signals
	wishbone2bus #(
		.AW(5),
		.DW(8),
		.SW(1),
	) wishbone2bus(
		.wb_cyc(slave_wb_cyc),
		.wb_stb(slave_wb_stb),
		.wb_we(slave_wb_we),
		.wb_adr(slave_wb_addr),
		.wb_dat_w(slave_wb_dat_w),
		.wb_dat_r(slave_wb_dat_r),
		.wb_ack(slave_wb_ack),
		.wb_err(slave_wb_err),
		.bus_wen(avalon_wen),
		.bus_ren(avalon_ren),
		.bus_adr(avalon_adr),
		.bus_wdt(avalon_wdt),
		.bus_rdt(avalon_rdt)
	);

	// Instantiate sockit OneWire module and connect signals
	sockit_owm #(
		.OVD_E(1),
		.CDR_E(1),
		.BDW(8),
		.BAW(5)
	) sockit_owm(
		.sys_clk_freq(sys_clk_freq),
		.clk(peripheral_clock),
		.rst(peripheral_reset),
		.bus_ren(avalon_ren),
		.bus_wen(avalon_wen),
		.bus_adr(avalon_adr),
		.bus_wdt(avalon_wdt),
		.bus_rdt(avalon_rdt),
		.bus_irq(slave_irq_o),
		.owr_p(owi_p),
		.owr_e(owi_oe),
		.owr_i(owi_i)
	);
endmodule
