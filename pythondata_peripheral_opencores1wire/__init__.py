import os.path
__dir__ = os.path.split(os.path.abspath(os.path.realpath(__file__)))[0]
data_location = os.path.join(__dir__, "rtl")
src = "https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-litex/pythondata-peripheral-opencores1wire"

def data_file(f):
    """Get absolute path for file inside pythondata_cpu_opencores1wire."""
    fn = os.path.join(data_location, f)
    fn = os.path.abspath(fn)
    if not os.path.exists(fn):
        raise IOError("File {f} doesn't exist in pythondata_peripheral_opencores1wire".format(f))
    return fn
